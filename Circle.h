/*
 * Circle.h
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_
class Circle {
public:
	Circle(double);
	~Circle();
	void setR(double);
	void setR(int);//setR overload olarak olusturuldu
	double getR()const;//const yapildi
	double calculateCircumference()const;//const yapildi
	double calculateArea()const;//const yapildi
	bool equal_to(Circle&)const; //fonksiyon eklendi
private:
	double r;
	double const PI = 3.14;//const yapildi
};
#endif /* CIRCLE_H_ */
