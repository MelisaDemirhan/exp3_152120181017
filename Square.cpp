/*
 * Square.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Square.h"

Square::Square(double a) {
	setA(a);
	setB(a);
}

Square::~Square() {
}

void Square::setA(double a) {
	this->a = a; //this->a eklendi
	this->b = a; //her kenari ayni olmasi icin
}

void Square::setB(double b) {
	this->b = b; //this->b eklendi
	this->a = b; //her kenari ayni olmasi icin
}

double Square::calculateCircumference() {
	return (a + b) * 2; //parantez eklendi
}

double Square::calculateArea() {
	return a * b;
}
