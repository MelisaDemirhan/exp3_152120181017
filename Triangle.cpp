/*
 * Triangle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Triangle.h"

Triangle::Triangle(double a, double b, double c) {
	setA(a);
	setB(b);
	setC(c);
}

Triangle::~Triangle() {

}

void Triangle::setA(double a) {
	this->a = a; //this->a eklendi
}

void Triangle::setB(double b) //degisken ismi verildi
{
	this->b = b; //this->b eklendi
}

void Triangle::setC(double c) //degisken ismi verildi
{
	this->c = c;//this->c eklendi
}

double Triangle::calculateCircumference() {
	return a + b + c;  // /2 kaldirildi
}

