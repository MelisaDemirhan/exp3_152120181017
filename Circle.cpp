/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include<iostream>
#include "Circle.h"
using namespace std;

Circle::Circle(double r) {
    setR(r);
}

Circle::~Circle() {
}

void Circle::setR(double r) {
    this->r = r;
}

void Circle::setR(int r)//setR overload olarak olusturuldu
{
    this->r = r;
}

double Circle::getR()const  //const yapildi
{
    return r;
}

double Circle::calculateCircumference()const  //const yapildi
{
    return PI * r * 2;
}

double Circle::calculateArea()const  //const yapildi
{
    return PI * r * r;
}

bool Circle::equal_to(Circle& c2)const {
    if (r == c2.r)//circle4 ve circle5'in r degerleri karsilastiriliyor
    {
        cout << "circle4 and circle5 are equal";
        return true;//ayni ise true
    }
    else
    {
        cout << "circle4 and circle5 are not equal";
        return false;//farkli ise false
    }

}
